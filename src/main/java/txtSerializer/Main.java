package txtSerializer;

import java.io.IOException;

/**
 * Main test
 * Created by alekseykravchenko on 16.12.14.
 */
public class Main {
    public static void main(String[] args)
            throws IllegalAccessException, NoSuchFieldException, IOException, InstantiationException, ClassNotFoundException {

        User user = new User("Aleksey", "Kravchenko");
        Serializator.serializeToTxt(user);
        String fileName = "/Users/alekseykravchenko/Desktop/temp.txt";
        User restoredUser = (User) Serializator.deserializeFromTxt(fileName);
    }
}
