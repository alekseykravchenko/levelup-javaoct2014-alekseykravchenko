package furniture;

/**
 * Класс Стул
 * Created by alekseykravchenko on 07.10.14.
 */
public class Chair extends Product {

    private boolean rotatingChair;

    public Chair(String title, int article, String color, double price, boolean rotatingChair) {
        super(title, article, color, price);
        this.rotatingChair = rotatingChair;
    }

    public void setRotatingChair(boolean rotatingChair) {
        this.rotatingChair = rotatingChair;
    }

    public boolean getRotatingChair() {
        return rotatingChair;
    }

    public String getRotateChair() {
        if (this.rotatingChair == true) {
            return "1. Вращать стул";
        } else return "1. Стул без вращения";
    }

    public void setRotateChair(boolean rotate) {
        System.out.println("1. Вращаю стул: " + rotate);
    }

    public String getRollChair() {
        return "2. Перекатить стул";
    }

    public void setRollChair(int distance) {
        System.out.println("2. Перекачиваю стул на " + distance + " метров");
    }

    public String getSitOnChair() {
        return "3. Сесть на стул";
    }

    public void setSitOnChair(boolean sit) {
        System.out.println("3. Сажусь на стул: " + sit);
    }


    @Override
    public String toString() {
        return "Название: " + getTitle() +
                "\nАртикул: " + getArticle() +
                "\nЦвет: " + getColor() +
                "\nВращение: " + getRotatingChair() +
                "\nЦена: " + getPrice();
    }

    @Override
    public void push() {
        System.out.println("Положить на стул");
    }

    @Override
    public void push(int weight) {
        if (weight > 100) {
            System.out.println("Слишком большой вес!");
        } else {
            System.out.println("Ложу на стул " + weight + " кг.");
        }
    }
}
