package furniture;

/**
 * Класс Комод
 * Created by alekseykravchenko on 07.10.14.
 */
public class Commode extends Product {

    private int numberOfBox;

    public Commode(String title, int article, String color, double price, int numberOfBox) {
        super(title, article, color, price);
        this.numberOfBox = numberOfBox;
    }

    public void setNumberOfBox(int numberOfBox) {
        this.numberOfBox = numberOfBox;
    }

    public int getNumberOfBox() {
        return numberOfBox;
    }

    public String getOpenBox() {
        return "1. Открыть ящик";
    }

    public void setOpenBox() {
        System.out.println("1. Открываю ящик");
    }

    public String getCloseBox() {
        return "2. Закрыть ящик";
    }

    public void setCloseBox() {
        System.out.println("2. Закрываю ящик");
    }

    public String getPushInBox() {
        return "3. Положить в ящик";
    }

    public void setPushInBox(String item) {
        System.out.println("3. Ложу в ящик " + item);
    }

    @Override
    public String toString() {
        return "Название: " + getTitle() +
                "\nАртикул: " + getArticle() +
                "\nЦвет: " + getColor() +
                "\nЯщики: " + getNumberOfBox() +
                "\nЦена: " + getPrice();
    }

    @Override
    public void push() {
        System.out.println("Положить на комод");
    }

    @Override
    public void push(int weight) {
        if (weight > 100) {
            System.out.println("Слишком большой вес!");
        } else {
            System.out.println("Ложу на комод " + weight + " кг.");
        }
    }
}
