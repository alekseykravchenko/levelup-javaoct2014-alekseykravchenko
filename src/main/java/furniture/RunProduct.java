package furniture;

/**
 * Точка входа в программу
 * Created by alekseykravchenko on 07.10.14.
 */
public class RunProduct {
    public static void main(String[] args) {

        System.out.println("Номенклатура:\n");
        Table table = new Table("Стол", 23345, "Ольха", 254.6, "Кухня");
        System.out.println(table.toString());
        System.out.println("Доступные действия со столом:");
        System.out.println(table.getPushOnTable());
        System.out.println(table.getMoveTable());
        System.out.println(table.getExpandTable());
        System.out.println("Выполняем действия со столом:");
        table.setPushOnTable("посуду");
        table.setMoveTable(10);
        table.setExpandTable(true);
        System.out.println("Overload-методы:");
        table.push();
        table.push(120);

        System.out.println();
        Chair chair = new Chair("Стул", 45234, "Бежевый", 110.0, true);
        System.out.println(chair.toString());
        System.out.println("Доступные действия со стулом:");
        System.out.println(chair.getRotateChair());
        System.out.println(chair.getRollChair());
        System.out.println(chair.getSitOnChair());
        System.out.println("Выполняем действия со стулом:");
        chair.setRotateChair(true);
        chair.setRollChair(12);
        chair.setSitOnChair(false);
        System.out.println("Overload-методы:");
        chair.push();
        chair.push(80);

        System.out.println();
        Commode commode = new Commode("Комод", 21324, "Белый", 352.5, 6);
        System.out.println(commode.toString());
        System.out.println("Доступные действия с комодом: ");
        System.out.println(commode.getOpenBox());
        System.out.println(commode.getCloseBox());
        System.out.println(commode.getPushInBox());
        System.out.println("Выполняем действия с комодом:");
        commode.setOpenBox();
        commode.setCloseBox();
        commode.setPushInBox("хлам");
        System.out.println("Overload-методы:");
        commode.push();
        commode.push(10);

    }
}
