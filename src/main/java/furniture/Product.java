package furniture;

/**
 * Абстрактный класс товара
 * Created by alekseykravchenko on 07.10.14.
 */
public abstract class Product {

    private String title;
    private int article;
    private String color;
    private double price;

    public Product() {

    }

    public Product(String title, int article, String color, double price) {
        this.title = title;
        this.article = article;
        this.color = color;
        this.price = price;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setArticle(int article) {
        this.article = article;
    }

    public int getArticle() {
        return article;
    }

    public abstract String toString();

    public abstract void push();

    public abstract void push(int weight);
}
