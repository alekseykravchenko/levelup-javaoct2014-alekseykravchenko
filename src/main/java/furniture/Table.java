package furniture;

/**
 * Класс Стол
 * Created by alekseykravchenko on 07.10.14.
 */
public class Table extends Product {

    private String destinationRoom;

    public Table() {

    }

    public Table(String name, int article, String color, double price, String destinationRoom) {
        super(name, article, color, price);
        this.destinationRoom = destinationRoom;
    }

    public void setDestinationRoom(String destinationRoom) {
        this.destinationRoom = destinationRoom;
    }

    public String getDestinationRoom() {
        return destinationRoom;
    }

    public String getPushOnTable() {
        return "1. Положить на стол";
    }

    public void setPushOnTable(String pushItem) {
        System.out.println("1. Ложу на стол " + pushItem);
    }

    public String getMoveTable() {
        return "2. Передвинуть стол";
    }

    public void setMoveTable(int distance) {
        System.out.println("2. Передвигаю стол на " + distance + " метров");
    }

    public String getExpandTable() {
        return "3. Разложить стол";
    }

    public void setExpandTable(boolean move) {
        System.out.println("3. Раскладываю стол: " + move);
    }

    @Override
    public String toString() {
        return "Название: " + getTitle() +
                "\nАртикул: " + getArticle() +
                "\nЦвет: " + getColor() +
                "\nНазначение: " + getDestinationRoom() +
                "\nЦена: " + getPrice();
    }

    @Override
    public void push() {
        System.out.println("Положить на стол");
    }

    @Override
    public void push(int weight) {
        if (weight > 100) {
            System.out.println("Слишком большой вес!");
        } else {
            System.out.println("Ложу на стол " + weight + " кг.");
        }
    }
}
