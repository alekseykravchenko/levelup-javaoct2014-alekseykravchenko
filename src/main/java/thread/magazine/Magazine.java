package thread.magazine;

/**
 * Created by alekseykravchenko on 13.12.14.
 */
public class Magazine {

    private final Object lock = new Object();
    private String message = "This is Sparta!";

    public static void main(String[] args) {
        Magazine magazine = new Magazine();
        magazine.doMagazine();
    }

    public void doMagazine() {
        Thread topicThread = new Thread(new Topic(message, lock), "Topic thread");
        Thread subscriber1 = new Thread(new Subscriber(message, lock), "Subscriber1 thread");
        Thread subscriber2 = new Thread(new Subscriber(message, lock), "Subscriber2 thread");
        Thread subscriber3 = new Thread(new Subscriber(message, lock), "Subscriber3 thread");
        Thread subscriber4 = new Thread(new Subscriber(message, lock), "Subscriber4 thread");
        topicThread.start();
        subscriber1.start();
        subscriber2.start();
        subscriber3.start();
        subscriber4.start();

    }

}
