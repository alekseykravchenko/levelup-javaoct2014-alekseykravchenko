package thread.magazine;

/**
 * Created by alekseykravchenko on 13.12.14.
 */
public class Topic implements Runnable {

    public static String message;
    private Object lock;

    public Topic(String message, Object lock) {
        this.message = message;
        this.lock = lock;
    }

    @Override
    public void run() {
        synchronized (lock) {
            try {
                System.out.println("Sleeping 5 sec.");
                Thread.sleep(5000);
                System.out.println("Message: " + message);
                lock.notifyAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
