package thread.magazine;

/**
 * Created by alekseykravchenko on 13.12.14.
 */
public class Subscriber implements Runnable {

    private String message = Topic.message;
    private Object lock;

    public Subscriber(String message, Object lock) {
        this.message = message;
        this.lock = lock;
    }

    @Override
    public void run() {
        synchronized (lock) {
            if (!message.isEmpty()) {
                System.out.println(Thread.currentThread().getName() + " taking a new message! " + message);
            } else {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
