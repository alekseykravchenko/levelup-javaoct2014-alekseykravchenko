package thread;

/**
 * Created by alekseykravchenko on 11.12.14.
 */
public class Counter {

    private int counter;

    public Counter() {
    }

    public Counter(int counter) {
        this.counter = counter;
    }

    public synchronized int increment() {
        return ++counter;
    }

}
