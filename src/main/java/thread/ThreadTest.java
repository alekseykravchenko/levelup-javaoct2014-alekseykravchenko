package thread;

/**
 * Created by alekseykravchenko on 11.12.14.
 */
public class ThreadTest {

    public static void main(String[] args) {

        Counter counter = new Counter();

        for (int i = 0; i < 10; i++) {
            Thread thread = new Thread(new Counter1(counter), "Thread" + i);
            thread.start();
        }
    }
}
