package holder;

/**
 * Created by alekseykravchenko on 30.10.14.
 */

public class Holder {

    public Holder next;

    public Integer data;

    public Holder(Integer data) {
        this.data = data;
    }

    public void setData(Integer newData) {
        this.data = newData;
    }

}

