package holder;

/**
 * Created by alekseykravchenko on 30.10.14.
 */
public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        HolderCollection holderCollection = new HolderCollection();
        holderCollection.add(5);
        holderCollection.add(4);
        holderCollection.add(7);
        holderCollection.add(10);
        holderCollection.add(25);
        holderCollection.add(16);
        holderCollection.add(19);
        holderCollection.add(5);
        holderCollection.add(34);
        //holderCollection.remove();
        //holderCollection.remove(1);
        holderCollection.add(3, 100);
        //System.out.println(holderCollection.get());
        //System.out.println(holderCollection.get(4));
        //System.out.println(holderCollection.contains(6));

//        System.out.println(holderCollection.size);
//        for (int i = 0; i < holderCollection.size; i++) {
//            System.out.println(holderCollection.get(i));
//        }
        for (Object i : holderCollection) {
            System.out.println(i);
        }
//        Iterator iter = holderCollection.iterator();
//        while (iter.hasNext()){
//            System.out.println(iter.next());
//        }
    }

}
