package holder;

import java.util.Iterator;

/**
 * Created by alekseykravchenko on 30.10.14.
 */
public class HolderCollection implements Iterable {
    public Holder head = null;
    public int size = 0;

    public void add(Integer data) {
        if (size == 0) {
            head = new Holder(data);
        } else {
            Holder temp = head;
            for (int i = 0; i < size - 1; i++) {
                temp = temp.next;
            }
            temp.next = new Holder(data);
        }
        size++;
    }

    public void add(int index, Integer data) {
        Holder temp = head;
        Holder temp2;
        if (size == 0) {
            head = new Holder(data);
//        } else if (index == 1) {
//            for (int i = 1; i < 2; i++) {
//                temp = temp.next;
//            }
//            temp2 = temp.next;
//            temp = new Holder(data);
//            temp.next = temp2;
        } else if (index > 1 || index < size) {
            for (int i = 1; i < index - 1; i++) {
                temp = temp.next;
            }
            temp2 = temp.next;
            temp.next = new Holder(data);
            temp.next.next = temp2;
        }
        size++;
    }

    public void remove() {
        Holder temp = head;
        if (head == null) {
            return;
        }
        for (int i = 0; i < size - 1; i++) {
            temp = temp.next;
        }
        temp.next = null;
        size--;
    }

    public void remove(int index) {
        Holder temp = head;
        if (index < 1 || index > size) {
            return;
        }
        if (index == 1) {
            head = temp.next;
        }
        for (int i = 1; i < index - 1; i++) {
            temp = temp.next;
        }
        temp.next = temp.next.next;
        size--;
    }

    public Integer get(int index) {
        Holder temp = head;
        for (int i = 0; i < size; i++) {
            if (index == i + 1) {
                return temp.data;
            } else {
                temp = temp.next;
            }
        }
        return null;
    }

    public Integer get() {
        Holder temp = head;
        for (int i = 0; i < size - 1; i++) {
            temp = temp.next;
        }
        return temp.data;
    }

    public boolean contains(Integer data) {
        Holder temp = head;
        for (int i = 0; i < size - 1; i++) {
            temp = temp.next;
            if (temp.data.equals(data)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator iterator() {
        return new HolderCollectionIterator();
    }

    private class HolderCollectionIterator implements Iterator {
        int currentPosition;

        @Override
        public boolean hasNext() {
            return currentPosition < size;
        }

        @Override
        public Integer next() {
            currentPosition++;
            return HolderCollection.this.get(currentPosition);
        }

        @Override
        public void remove() {

        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
