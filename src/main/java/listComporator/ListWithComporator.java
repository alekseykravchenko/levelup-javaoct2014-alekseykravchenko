package listComporator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by alekseykravchenko on 04.11.14.
 */
public class ListWithComporator {
    public static void main(String[] args) {

        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        arrayList.add(10);
        arrayList.add(33);
        arrayList.add(123);
        arrayList.add(12);
        arrayList.add(36);
        arrayList.add(1);

        System.out.println(arrayList);
        Collections.sort(arrayList, new MyComparator());
        for (Object e : arrayList) {
            System.out.println(e);
        }
    }

    private static class MyComparator implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            return (o1 % 33) - (o2 % 33);
        }
    }
}
