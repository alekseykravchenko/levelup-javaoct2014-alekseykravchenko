package orm;

import java.sql.Date;

/**
 * Student
 * Created by alekseykravchenko on 09.12.14.
 */
@Table(name = "Student")
public class Student {

    @Column(name = "id")
    private Long id;
    @Column(name = "firstName")
    private String firstName;
    @Column(name = "lastName")
    private String lastName;
    @Column(name = "birthDate")
    private Date birthDate;
    @Column(name = "group_Id")
    private Long group_Id;

    public Student() {
    }

    public Student(Long id, String firstName, String lastName, Date birthDate, Long group_Id) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
        this.group_Id = group_Id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Long getGroup_Id() {
        return group_Id;
    }

    public void setGroup_Id(Long group_Id) {
        this.group_Id = group_Id;
    }
}
