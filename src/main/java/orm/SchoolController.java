package orm;

import java.sql.*;

/**
 * Created by alekseykravchenko on 09.12.14.
 */
public class SchoolController {

    private DBEngine dbEngine = DBEngine.getInstance();
    private Connection connection;
    private PreparedStatement groupSelectStatement;
    private PreparedStatement studentSelectStatement;
    private PreparedStatement groupInsertStatement;
    private PreparedStatement studentInsertStatement;

    public SchoolController() {
        try {
            connection = dbEngine.getConnection();
            groupSelectStatement = connection.prepareStatement(Queries.createSelectQuery(Group.class));
            studentSelectStatement = connection.prepareStatement(Queries.createSelectQuery(Student.class));
            groupInsertStatement = connection.prepareStatement(Queries.createInsertQuery(Group.class)
                    + "?);", Statement.RETURN_GENERATED_KEYS);
            studentInsertStatement = connection.prepareStatement(Queries.createInsertQuery(Student.class)
                    + "?, ?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void selectGroup() {
        try {
            ResultSet rs = groupSelectStatement.executeQuery();
            while (rs.next()) {
                System.out.print(rs.getLong("id") + " ");
                System.out.println(rs.getString("groupName"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void selectStudent() {
        ResultSet rs;
        try {
            rs = studentSelectStatement.executeQuery();
            while (rs.next()) {
                System.out.print(rs.getString("id") + " ");
                System.out.print(rs.getString("firstName") + " ");
                System.out.print(rs.getString("lastName") + " ");
                System.out.print(rs.getDate("birthDate") + " ");
                System.out.println(rs.getInt("group_Id") + " ");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createGroup(String groupName) throws SQLException {
        Group group = new Group();
        group.setGroupName(groupName);
        Long group_Id = saveGroupToDataBase(group);
    }

    public Long saveGroupToDataBase(Group group) throws SQLException {
        Long id = null;
        groupInsertStatement.setString(1, group.getGroupName());
        groupInsertStatement.executeUpdate();
        ResultSet resultSet = groupInsertStatement.getGeneratedKeys();
        if (resultSet.next()) {
            id = resultSet.getLong(1);
        }
        return id;
    }

    public void createStudent(String firstName, String lastName, java.sql.Date birthDate, Long group_Id)
            throws SQLException {
        Student student = new Student();
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setBirthDate(birthDate);
        student.setGroup_Id(group_Id);
        Long student_Id = saveStudentToDataBase(student);
    }

    public Long saveStudentToDataBase(Student student) throws SQLException {
        Long id = null;
        studentInsertStatement.setString(1, student.getFirstName());
        studentInsertStatement.setString(2, student.getLastName());
        studentInsertStatement.setDate(3, student.getBirthDate());
        studentInsertStatement.setLong(4, student.getGroup_Id());
        studentInsertStatement.executeUpdate();
        ResultSet resultSet = studentInsertStatement.getGeneratedKeys();
        if (resultSet.next()) {
            id = resultSet.getLong(1);
        }
        return id;
    }

    public void closeConnection() {
        try {
            groupInsertStatement.close();
            studentInsertStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
