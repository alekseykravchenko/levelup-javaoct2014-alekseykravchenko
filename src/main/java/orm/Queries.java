package orm;

import java.lang.reflect.Field;

/**
 * Queries
 * Created by alekseykravchenko on 09.12.14.
 */
public class Queries {

    public static String createSelectQuery(Class clazz)
            throws IllegalAccessException, ClassNotFoundException, InstantiationException {
        return "SELECT id, " + getColumnName(clazz) + " FROM " + "levelup." + getTableName(clazz);
    }

    public static String createInsertQuery(Class clazz)
            throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        return "INSERT INTO levelup." + getTableName(clazz) + " (" + (getColumnName(clazz)) + ")" +
                " VALUES (";
    }

    public static String getTableName(Class clazz)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Object obj = Class.forName(clazz.getName()).newInstance();
        return obj.getClass().getAnnotation(Table.class).name();
    }

    public static String getColumnName(Class clazz)
            throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        Object obj = Class.forName(clazz.getName()).newInstance();
        Field[] fields = obj.getClass().getDeclaredFields();
        String columns = "";
        for (Field field : fields) {
            if (field.getAnnotation(Column.class).name().equals("id")) {
                columns += "";
            } else {
                columns += field.getAnnotation(Column.class).name() + ", ";
            }
        }
        return columns.substring(0, (columns.length() - 2));
    }
}
