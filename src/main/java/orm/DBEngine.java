package orm;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * DBEngine
 * Created by alekseykravchenko on 09.12.14.
 */
public class DBEngine {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/levelup?useUnicode=true&characterEncoding=utf8";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "qwerty123";
    private static final String DB_DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static DBEngine instance;

    private DBEngine() {
        try {
            Class.forName(DB_DRIVER_NAME);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static DBEngine getInstance() {
        if (instance == null) {
            instance = new DBEngine();
        }
        return instance;
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
    }
}
