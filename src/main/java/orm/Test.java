package orm;

import java.sql.Date;
import java.sql.SQLException;

/**
 * Test
 * Created by alekseykravchenko on 09.12.14.
 */
public class Test {

    public static void main(String[] args) throws SQLException {
        SchoolController sc = new SchoolController();
        sc.createGroup("Java");
        sc.createStudent("John", "Glenn", new Date(new java.util.Date().getTime()), 1L);
        sc.selectGroup();
        sc.selectStudent();
        sc.closeConnection();
    }
}
