package orm;

/**
 * Group
 * Created by alekseykravchenko on 09.12.14.
 */
@Table(name = "Group")
public class Group {

    @Column(name = "id")
    private Long id;

    @Column(name = "groupName")
    private String groupName;

    public Group() {
    }

    public Group(Long id, String groupName) {
        this.id = id;
        this.groupName = groupName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
