package greeting;

/**
 * Клас приветствия на Французском
 * Created by alekseykravchenko on 21.10.14.
 */
public class GreetingFrench implements Greeting {

    public void greet() {
        System.out.println("Bonjour Ivan!");
    }

    public void greetWithName(String name) {
        System.out.println("Bonjour" + name);
    }
}
