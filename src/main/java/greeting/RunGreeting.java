package greeting;

/**
 * Запуск
 * Created by alekseykravchenko on 21.10.14.
 */
public class RunGreeting {

    public static void getGreet(Greeting greeting) {
        greeting.greet();
        greeting.greetWithName(" Aleksey!");
    }

    public static void main(String[] args) {
        Greeting english = new GreetingEnglish();
        Greeting french = new GreetingFrench();
        getGreet(english);
        getGreet(french);

        Greeting spanish = new Greeting() {
            @Override
            public void greet() {
                System.out.println("Hola Ivan!");
            }

            @Override
            public void greetWithName(String name) {
                System.out.println("Hola" + name);
            }
        };

        getGreet(spanish);

    }
}
