package greeting;

/**
 * Класс Приветствие на Английскосм
 * Created by alekseykravchenko on 21.10.14.
 */
public class GreetingEnglish implements Greeting {

    public void greet() {
        System.out.println("Hello Ivan!");
    }

    public void greetWithName(String name) {
        System.out.println("Hello" + name);
    }
}
