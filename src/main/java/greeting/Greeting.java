package greeting;

/**
 * Интерфейс Приветствие
 * Created by alekseykravchenko on 21.10.14.
 */
public interface Greeting {
    void greet();
    void greetWithName(String name);
}
