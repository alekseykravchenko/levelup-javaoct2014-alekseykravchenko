package io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Stack;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *   _______________________________________
 *  /Найти в папках и во вложенных папках   \
 *  \все файлы zip и показать их содержимое /
 *   \_____________________________________/
 *           \   ^__^
 *            \  (oo)\______
 *               (__)\      )\/\
 *                   ||———w |
 *                   ||    ||
 * Created by alekseykravchenko on 14.11.14.
 */
public class Unzip {
    private static void unzipFiles(File dir) throws IOException {
        Stack<File> stack = new Stack<File>();
        stack.push(dir);
        while (!stack.isEmpty()) {
            File child = stack.pop();
            if (child.isDirectory()) {
                for (File f : child.listFiles()) {
                    stack.push(f);
                }
            } else if (child.isFile()) {
                if (child.getName().contains(".zip")) {
                    System.out.println("======" + child.getName() + "======");
                    String zipname = child.getPath();
                    ZipInputStream zin = new ZipInputStream(new FileInputStream(zipname));
                    ZipEntry entry;
                    while ((entry = zin.getNextEntry()) != null) {
                        System.out.println(entry.getName());
                        zin.closeEntry();
                    }
                    zin.close();
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        File dir = new File("/Users/alekseykravchenko/Desktop/");
        unzipFiles(dir);
    }
}
