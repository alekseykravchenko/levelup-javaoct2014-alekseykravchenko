package io;

import java.io.*;

/**
 * Придумать механизм шифровки/дешифровки
 * Программа запускается с одним из следующих наборов параметров:
 * -e fileName fileOutputName
 * -d fileName fileOutputName
 * где
 * fileName - имя файла, который необходимо зашифровать/расшифровать
 * fileOutputName - имя файла, куда необходимо записать результат шифрования/дешифрования
 * -e - ключ указывает, что необходимо зашифровать данные
 * -d - ключ указывает, что необходимо расшифровать данные
 * Created by alekseykravchenko on 12.11.14.
 */
public class EncodeDecode {
    public static File encode(File inputFile, File outputFile) throws IOException {
        String key = "key";
        byte[] keyArray = key.getBytes();
        // хаваем файл в массив байтов
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(inputFile));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int data;
        while ((data = bis.read()) != -1) {
            baos.write(data);
        }
        byte[] fileByteArray = baos.toByteArray();
        byte[] res = new byte[fileByteArray.length];
        for (int i = 0; i < fileByteArray.length; i++) {
            res[i] = (byte) (fileByteArray[i] ^ keyArray[i % keyArray.length]);
        }
        FileOutputStream resFile = new FileOutputStream(outputFile);
        for (int i = 0; i < res.length; i++) {
            resFile.write(res[i]);
        }
        return outputFile;
    }

    public static File decode(File inputFile, File outputFile) throws IOException {
        String key = "key";
        byte[] keyArray = key.getBytes();
        // хаваем файл в массив байтов
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(inputFile));
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int data;
        while ((data = bis.read()) != -1) {
            baos.write(data);
        }
        byte[] fileByteArray = baos.toByteArray();
        byte[] res = new byte[fileByteArray.length];
        for (int i = 0; i < fileByteArray.length; i++) {
            res[i] = (byte) (fileByteArray[i] ^ keyArray[i % keyArray.length]);
        }
        FileOutputStream resFile = new FileOutputStream(outputFile);
        for (int i = 0; i < res.length; i++) {
            resFile.write(res[i]);
        }
        return outputFile;
    }

    public static void main(String[] args) throws IOException {
        // зашифровать
        if (args[0].equals("-e")) {
            if (args.length != 3 && args.length < 3) {
                System.out.println("Введите авргументы.");
                System.exit(1);
            } else {
                File inputFile = new File(args[1]);
                File outputFile = new File(args[2]);
                encode(inputFile, outputFile);
            }
        }
        // расшифровать
        if (args[0].equals("-d")) {
            if (args.length != 3 && args.length < 3) {
                System.out.println("Введите авргументы.");
                System.exit(1);
            } else {
                File inputFile = new File(args[1]);
                File outputFile = new File(args[2]);
                decode(outputFile, inputFile);
            }
        }
    }
}
