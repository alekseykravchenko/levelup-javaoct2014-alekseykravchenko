package io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeSet;

/**
 *   ^__^
 *   (oo)\______
 *   (__)\      )\/\
 *       ||———w |
 *       ||    ||
 * Программа запускается с одним параметром - именем файла, который содержит английский текст.
 * Посчитать частоту встречания каждого символа.
 * Отсортировать результат по возрастанию кода ASCII. Пример: ','=44, 's'=115, 't'=116
 * Вывести на консоль отсортированный результат:
 * [символ1]  частота1
 * [символ2]  частота2
 * Закрыть потоки
 * Created by alekseykravchenko on 12.11.14.
 */
public class SymbolCounter {
    public static void main(String[] args) throws IOException {

        BufferedReader in = new BufferedReader(new FileReader(args[0]));
        int c;
        int[] occurances = new int[256];
        TreeSet<Character> symbols = new TreeSet<Character>();
        while ((c = in.read()) != -1) {
            symbols.add((char) c);
            occurances[c] = occurances[c] + 1;
        }
        for (int e : symbols) {
            System.out.println("символ " + (char) e + " встречался " + occurances[e] + " раз");
        }
        in.close();
    }
}
