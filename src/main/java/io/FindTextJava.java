package io;

import java.io.*;

/**
 *   ____________________________
 *  /Найти строки со словом java \
 *  \____________________________/
 *           \   ^__^
 *            \  (oo)\______
 *               (__)\      )\/\
 *                   ||———w |
 *                   ||    ||
 * Created by alekseykravchenko on 13.11.14.
 */

public class FindTextJava {

    public static void findJava(String fileName) throws IOException {
        BufferedReader in = new BufferedReader(new FileReader(fileName));
        FileWriter out = new FileWriter(new File("/Users/alekseykravchenko/Desktop/result2.txt"));
        String s;
        while ((s = in.readLine()) != null) {
            if (s.contains("java")) {
                System.out.println(s);
                out.write(s + "\n");
            }
            out.flush();
        }
        in.close();
    }

    public static void main(String[] args) throws IOException {
        String fileName = "/Users/alekseykravchenko/Desktop/test333.txt";
        File file = new File(fileName);
        if (file.exists()) {
            findJava(fileName);
        } else {
            file.createNewFile();
        }
    }
}
