package reflection;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

/**
 * Reflection API HashMap
 * Created by alekseykravchenko on 20.11.14.
 */
public class ReflectionMap {
    public static void main(String[] args) {

        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("Horstmann", new Book("Cay Horstmann", "Java"));
        hashMap.put("Shildt", new Book("Gerbert Shildt", "Full Java"));
        hashMap.put("Eckel", new Book("Bruce Eckel", "Thinking in Java"));

        try {
            // Key=Horstmann
            Class aClass = Class.forName(hashMap.get("Horstmann").getClass().getName());
            Object Horstmann = aClass.newInstance();
            Class[] aParameters = new Class[]{String.class, String.class};
            Method aMethod = aClass.getMethod("createBook", aParameters);
            Object[] aArguments = new Object[]{((Book) hashMap.get("Horstmann")).getAuthor(),
                    ((Book) hashMap.get("Horstmann")).getName()};
            aMethod.invoke(Horstmann, aArguments);
            Book horstmann = (Book) Horstmann;
            System.out.println("1 Книга:");
            System.out.println(horstmann.getAuthor());
            System.out.println(horstmann.getName());

            // Key=Shildt
            Class bClass = Class.forName(hashMap.get("Shildt").getClass().getName());
            Object Shildt = bClass.newInstance();
            Class[] bParameters = new Class[]{String.class, String.class};
            Method bMethod = bClass.getMethod("createBook", bParameters);
            Object[] bArguments = new Object[]{((Book) hashMap.get("Shildt")).getAuthor(),
                    ((Book) hashMap.get("Shildt")).getName()};
            bMethod.invoke(Shildt, bArguments);
            Book shildt = (Book) Shildt;
            System.out.println("\n2 Книга: ");
            System.out.println(shildt.getAuthor());
            System.out.println(shildt.getName());

            // Key=Eckel
            Class cClass = Class.forName(hashMap.get("Eckel").getClass().getName());
            Object Eckel = cClass.newInstance();
            Class[] cParameters = new Class[]{String.class, String.class};
            Method cMethod = cClass.getMethod("createBook", cParameters);
            Object[] cArguments = new Object[]{((Book) hashMap.get("Eckel")).getAuthor(),
                    ((Book) hashMap.get("Eckel")).getName()};
            cMethod.invoke(Eckel, cArguments);
            Book eckel = (Book) Eckel;
            System.out.println("\n3 Книга: ");
            System.out.println(eckel.getAuthor());
            System.out.println(eckel.getName());


        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }
}
