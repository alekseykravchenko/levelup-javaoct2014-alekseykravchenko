package reflection;

import java.lang.reflect.Field;

/**
 * Reflection API fields
 * Created by alekseykravchenko on 20.11.14.
 */
public class ReflectionFields {
    public static void main(String[] args) {
        try {
            // Reader
            Class reader = Class.forName("reflection.Reader");
            Object myReader = reader.newInstance();

            Field[] readerFields = reader.getDeclaredFields();
            for (Field readerField : readerFields) {
                System.out.println(readerField);
            }

            Field readerFirstName = reader.getDeclaredField("firstName");
            readerFirstName.setAccessible(true);
            readerFirstName.set(myReader, "Ivan");

            Field readerLastName = reader.getDeclaredField("lastName");
            readerLastName.setAccessible(true);
            readerLastName.set(myReader, "Ivanov");

            Field readerAge = reader.getDeclaredField("age");
            readerAge.setAccessible(true);
            readerAge.setInt(myReader, 22);

            Field readerBook = reader.getDeclaredField("book");
            readerBook.setAccessible(true);
            readerBook.set(myReader, new Book());


            // Book
            Class book = Class.forName("reflection.Book");
            Object myBook = book.newInstance();

            Field[] bookFields = book.getDeclaredFields();
            for (Field bookField : bookFields) {
                System.out.println(bookField);
            }

            Field bookAuthor = book.getDeclaredField("author");
            bookAuthor.setAccessible(true);
            bookAuthor.set(myBook, "Герберт Шилдт");

            Field bookName = book.getDeclaredField("name");
            bookName.setAccessible(true);
            bookName.set(myBook, "Java Полное руководство");

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
