package reflection;

/**
 * Created by alekseykravchenko on 20.11.14.
 */
public class Reader {
    private String firstName;
    private String lastName;
    private int age;
    private Book book;

    public Reader() {
    }

    public Reader(String firstName, String lastName, int age, Book book) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.book = book;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }
}
