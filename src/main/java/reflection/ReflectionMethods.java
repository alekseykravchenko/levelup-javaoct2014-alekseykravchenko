package reflection;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * Reflection API methods
 * Created by alekseykravchenko on 20.11.14.
 */
public class ReflectionMethods {
    public static void main(String[] args) {
        try {
            // Reader
            Class reader = Class.forName("reflection.Reader");
            Object myReader = reader.newInstance();
            Field readerFirstName = reader.getDeclaredField("firstName");
            readerFirstName.setAccessible(true);
            readerFirstName.set(myReader, "Petr");

            Field readerLastName = reader.getDeclaredField("lastName");
            readerLastName.setAccessible(true);
            readerLastName.set(myReader, "Petrov");

            Field readerAge = reader.getDeclaredField("age");
            readerAge.setAccessible(true);
            readerAge.setInt(myReader, 25);

            Field readerBook = reader.getDeclaredField("book");
            readerBook.setAccessible(true);
            readerBook.set(myReader, new Book());

            // Book
            Class book = Class.forName("reflection.Book");
            Object myBook = book.newInstance();
            Class[] parameters = new Class[] { String.class, String.class };
            Method bookMethod = book.getMethod("createBook", parameters);
            Object[] arguments = new Object[] { new String("Cay Horstmann"), new String("Java") };
            bookMethod.invoke(myBook, arguments);


        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
}
