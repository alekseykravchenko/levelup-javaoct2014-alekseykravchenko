package reflection;

/**
 * Created by alekseykravchenko on 20.11.14.
 */
public class Book {
    private String author;
    private String name;

    public Book() {
    }

    public Book(String author, String name) {
        this.author = author;
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void createBook(String author, String name) {
        this.author = author;
        this.name = name;
    }
}
