package computer;

/**
 * Created by alekseykravchenko on 27.10.14.
 */
public class Computer {

    private CPUamd cpu = new CPUamd();
    private HDDwd hddWd = new HDDwd();
    private SoundAti soundAti = new SoundAti();
    private VideoRadeon videoRadeon = new VideoRadeon();

    public void calCulate() {
        cpu.calculate();
    }

    public void writeData() {
        hddWd.writeData();
    }

    public void playSound() {
        soundAti.playSound();
    }

    public void showVideo() {
        videoRadeon.showDisplay();
    }

    public static void main(String[] args) {
        Computer computerOne = new Computer();
        computerOne.calCulate();
        computerOne.writeData();
        computerOne.playSound();
        computerOne.showVideo();
    }
}
