package fileNameFilter;

import java.io.*;

/**
 * Копирование каталога и его содержимого.
 * Флаг указывает, нужно ли копировать подкаталоги.
 * Created by alekseykravchenko on 17.11.14.
 */
public class CopyByFlag {
    public static void copy(File source, File destination, boolean includeSub) throws IOException {
        // копирование содержимого каталога и подкаталогов
        if (includeSub) {
            if (source.isDirectory()) {
                if (!destination.exists()) {
                    destination.mkdir();
                }
                // записать все файлы по фильтру в массив
                String[] files = source.list(new FilterImage());

                for (String file : files) {
                    //construct the source and destination file structure
                    File srcFile = new File(source, file);
                    File destFile = new File(destination, file);
                    // рекурсия
                    copy(srcFile, destFile, true);
                }
            } else {
                // Буферизрованное побайтовое копирование
                BufferedInputStream in = new BufferedInputStream(new FileInputStream(source));
                BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(destination));
                byte[] b = source.getName().getBytes();
                while ((in.read(b)) != -1) {
                    out.write(b);
                }
                in.close();
                out.flush();
                out.close();
            }
            // копирование только содержимого основного каталога
        } else if (!includeSub) {
            if (!destination.exists()) {
                destination.mkdirs();
            }
            // записать все файлы по фильтру в массив
            String[] files = source.list(new FilterImage());
            for (String file : files) {
                // Буферизрованное побайтовое копирование
                BufferedInputStream in = new BufferedInputStream(
                        new FileInputStream(source.getAbsolutePath() + "/" + file));
                BufferedOutputStream out = new BufferedOutputStream(
                        new FileOutputStream(destination.getAbsolutePath() + "/" + file));
                byte[] b = source.getName().getBytes();
                while ((in.read(b)) != -1) {
                    out.write(b);
                }
                in.close();
                out.flush();
                out.close();
            }
        }
    }
}
