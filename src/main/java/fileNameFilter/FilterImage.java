package fileNameFilter;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Фильтр файлов с именем image
 * Created by alekseykravchenko on 15.11.14.
 */
public class FilterImage implements FilenameFilter {

    @Override
    public boolean accept(File dir, String name) {
        String lowercaseName = name.toLowerCase();
        if (lowercaseName.contains("image")) {
            return true;
        } else {
            return false;
        }
    }
}
