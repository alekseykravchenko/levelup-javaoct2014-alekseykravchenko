package fileNameFilter;

import java.io.File;
import java.io.IOException;

/**
 * 1. Вывести на экран только файлы, которые содержат в имени "image"
 * 2. Скопировать файлы с подкататалогами или без - учитывая флаг
 * Created by alekseykravchenko on 15.11.14.
 */
public class Run {

    public static void main(String[] args) {
        File source = new File("/Users/alekseykravchenko/Desktop/myDirectory/");
        File destination = new File("/Users/alekseykravchenko/Desktop/myDirectory33/");

        if (!source.exists()) {
            System.exit(0);
        } else {
            try {
                CopyByFlag.copy(source, destination, false);
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
        System.out.println("Вуаля :)");
    }
}
