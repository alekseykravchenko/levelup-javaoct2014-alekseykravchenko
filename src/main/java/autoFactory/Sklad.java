package autoFactory;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Sklad
 * Created by alekseykravchenko on 25.11.14.
 */
public class Sklad {

    public static HashMap<String, LinkedList<Part>> createParts() {
        // engine
        LinkedList<Part> engine = new LinkedList<>();
        engine.add(new Part("1111"));
        engine.add(new Part("1112"));
        engine.add(new Part("1113"));
        engine.add(new Part("1114"));

        // transmission
        LinkedList<Part> transmission = new LinkedList<>();
        transmission.add(new Part("2222"));
        transmission.add(new Part("2223"));
        transmission.add(new Part("2224"));
        transmission.add(new Part("2225"));

        // wheels
        LinkedList<Part> wheels = new LinkedList<>();
        wheels.add(new Part("3333"));
        wheels.add(new Part("3334"));
        wheels.add(new Part("3335"));
        wheels.add(new Part("3336"));

        HashMap<String, LinkedList<Part>> sklad = new HashMap<>();
        sklad.put("engine", engine);
        sklad.put("transmission", transmission);
        sklad.put("wheels", wheels);

        return sklad;
    }
}
