package autoFactory;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Run
 * Created by alekseykravchenko on 25.11.14.
 */
public class Factory {
    public static void main(String[] args) {
        HashMap<String, LinkedList<autoFactory.Part>> sklad = Sklad.createParts();
        createToyota(sklad);
        createBMW(sklad);
        createHonda(sklad);
    }

    public static void createToyota(HashMap<String, LinkedList<Part>> sklad) {
        try {
            Class aToyota = Class.forName("autoFactory.Toyota");
            Object toyota = aToyota.newInstance();
            Field toyotaModelField = aToyota.getDeclaredField("model");
            toyotaModelField.setAccessible(true);
            toyotaModelField.set(toyota, "Highlander");
            addEngine(aToyota, toyota, sklad);
            addTransmission(aToyota, toyota, sklad);
            addWheels(aToyota, toyota, sklad);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void createBMW(HashMap<String, LinkedList<Part>> sklad) {
        try {
            Class aBMW = Class.forName("autoFactory.BMW");
            Object BMW = aBMW.newInstance();
            Field bmwModelField = aBMW.getDeclaredField("model");
            bmwModelField.setAccessible(true);
            bmwModelField.set(BMW, "M3");
            addEngine(aBMW, BMW, sklad);
            addTransmission(aBMW, BMW, sklad);
            addWheels(aBMW, BMW, sklad);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void createHonda(HashMap<String, LinkedList<Part>> sklad) {
        try {
            Class aHonda = Class.forName("autoFactory.Honda");
            Object Honda = aHonda.newInstance();
            Field hondaModelField = aHonda.getDeclaredField("model");
            hondaModelField.setAccessible(true);
            hondaModelField.set(Honda, "Civic");
            addEngine(aHonda, Honda, sklad);
            addTransmission(aHonda, Honda, sklad);
            addWheels(aHonda, Honda, sklad);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private static void addEngine(Class aClass, Object object, HashMap<String, LinkedList<Part>> sklad) {
        try {
            Field engineField = aClass.getDeclaredField("engine");
            engineField.setAccessible(true);
            LinkedList<Part> engines = sklad.get("engine");
            if ((!engines.isEmpty()) && checkApplicable(engineField, engines)) {
                engineField.set(object, engines.pop());
            } else if (engines.isEmpty()) {
                System.out.println("Недостаточно двигателей!");
                System.exit(1);
            } else if (!checkApplicable(engineField, engines)) {
                System.out.println("Эти двигатели не подходят!");
                System.exit(1);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private static void addTransmission(Class aClass, Object object, HashMap<String, LinkedList<Part>> sklad) {
        try {
            Field transmissionField = aClass.getDeclaredField("transmission");
            transmissionField.setAccessible(true);
            LinkedList<Part> transmission = sklad.get("transmission");
            if ((!transmission.isEmpty()) && checkApplicable(transmissionField, transmission)) {
                transmissionField.set(object, transmission.pop());
            } else if (transmission.isEmpty()) {
                System.out.println("Недостаточно трансмиссий!");
                System.exit(1);
            } else if (!checkApplicable(transmissionField, transmission)) {
                System.out.println("Эта трансмиссия не подходит!");
                System.exit(1);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private static void addWheels(Class aClass, Object object, HashMap<String, LinkedList<Part>> sklad) {
        try {
            Field transmissionField = aClass.getDeclaredField("wheels");
            transmissionField.setAccessible(true);
            LinkedList<Part> wheels = sklad.get("wheels");
            if ((!wheels.isEmpty()) && checkApplicable(transmissionField, wheels)) {
                transmissionField.set(object, wheels.pop());
            } else if (wheels.isEmpty()) {
                System.out.println("Недостаточно колес!");
                System.exit(1);
            } else if (!checkApplicable(transmissionField, wheels)) {
                System.out.println("Эти колеса не подходят!");
                System.exit(1);
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    private static boolean checkApplicable(Field field, LinkedList<Part> detail) {
        Annotation annotation = field.getAnnotation(Applicable.class);
        for (String value : ((Applicable) annotation).details()) {
            for (Part part : detail) {
                if (part.getPartCode().equals(value)) {
                    return true;
                }
            }
        }
        return false;
    }
}
