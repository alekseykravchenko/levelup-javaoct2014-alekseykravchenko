package autoFactory;

/**
 * Part Class
 * Created by alekseykravchenko on 25.11.14.
 */
public class Part {

    private String partCode;

    public Part() {
    }

    public Part(String partCode) {
        this.partCode = partCode;
    }

    public String getPartCode() {
        return partCode;
    }

    public void setPartCode(String partCode) {
        this.partCode = partCode;
    }
}
