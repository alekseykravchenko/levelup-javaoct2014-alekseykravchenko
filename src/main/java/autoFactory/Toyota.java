package autoFactory;

/**
 * Toyota
 * Created by alekseykravchenko on 25.11.14.
 */
public class Toyota {

    @Applicable(details = {"1111", "1112", "1113", "1114"})
    private Part engine;

    @Applicable(details = {"2222", "2223", "2224", "2225"})
    private Part transmission;

    @Applicable(details = {"3333", "3334", "3335", "3336"})
    private Part wheels;

    private String model;

    public Toyota() {
    }

    public Toyota(Part engine, Part transmission, Part wheels, String model) {
        this.engine = engine;
        this.transmission = transmission;
        this.wheels = wheels;
        this.model = model;
    }

    public Part getEngine() {
        return engine;
    }

    public void setEngine(Part engine) {
        this.engine = engine;
    }

    public Part getTransmission() {
        return transmission;
    }

    public void setTransmission(Part transmission) {
        this.transmission = transmission;
    }

    public Part getWheels() {
        return wheels;
    }

    public void setWheels(Part wheels) {
        this.wheels = wheels;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
