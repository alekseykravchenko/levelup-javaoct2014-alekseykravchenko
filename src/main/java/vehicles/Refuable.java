package vehicles;

/**
 * Created by alekseykravchenko on 16.10.14.
 */
public interface Refuable {

    void refuel(int liters);
}
