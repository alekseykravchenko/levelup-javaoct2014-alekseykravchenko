package vehicles;

/**
 * Created by alekseykravchenko on 16.10.14.
 */
public interface Moveable {

    void move(int lat, int lng);

    void stop();
}
