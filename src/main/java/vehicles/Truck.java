package vehicles;

/**
 * Created by alekseykravchenko on 16.10.14.
 */
public class Truck extends Vehicle {

    private int passengers;

    public Truck() {
    }

    public Truck(int weight, int height, int width, int wheelCount, float speed, int passengers) {
        super(weight, height, width, wheelCount, speed);
        this.passengers = passengers;
    }

    public int getPassengers() {
        return passengers;
    }

    public void setPassengers(int passengers) {
        this.passengers = passengers;
    }

    @Override
    public void aging(int years) {
        System.out.println("Aging to: " + years);
    }

    @Override
    public void changePart(int number) {
        System.out.println("Changing to: " + number);
    }

    @Override
    public void move(int lat, int lng) {
        System.out.println("Moving to: " + lat + ", " + lng);
    }

    @Override
    public void stop() {
        System.out.println("Stopping.");
    }

    @Override
    public void refuel(int liters) {
        System.out.println("Refuel: " + liters);
    }

    public void loadTruck(int kgLoad) {
        System.out.println("Loading " + kgLoad + " kg");
    }
}
