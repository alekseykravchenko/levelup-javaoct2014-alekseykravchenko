package vehicles;

/**
 * Created by alekseykravchenko on 16.10.14.
 */
public class Car extends Vehicle {

    private int doorCount;

    public Car() {
    }

    public Car(int weight, int height, int width, int wheelCount, float speed, int doorCount) {
        super(weight, height, width, wheelCount, speed);
        this.doorCount = doorCount;
    }

    public int getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(int doorCount) {
        this.doorCount = doorCount;
    }

    @Override
    public void aging(int years) {
        System.out.println("Aging to: " + years);
    }

    @Override
    public void changePart(int number) {
        System.out.println("Changing to: " + number);
    }

    @Override
    public void move(int lat, int lng) {
        System.out.println("Moving to: " + lat + ", " + lng);
    }

    @Override
    public void stop() {
        System.out.println("Stopping.");
    }

    @Override
    public void refuel(int liters) {
        System.out.println("Refuel: " + liters);
    }

    public void addPassengers(String passenger) {
        System.out.println("Adding " + passenger);
    }
}
