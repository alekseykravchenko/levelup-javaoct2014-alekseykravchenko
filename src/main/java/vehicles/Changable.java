package vehicles;

/**
 * Created by alekseykravchenko on 16.10.14.
 */
public interface Changable {

    void changePart(int number);
}
