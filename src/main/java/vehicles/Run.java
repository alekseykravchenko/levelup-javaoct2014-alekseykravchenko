package vehicles;

/**
 * Created by alekseykravchenko on 16.10.14.
 */
public class Run {

    public static void main(String[] args) {
        Vehicle[] cars = new Vehicle[4];
        cars[0] = new Car();
        cars[1] = new Car();
        cars[2] = new Bike();
        cars[3] = new Truck();

        int[] lat = {12, 34, 56, 44};
        int[] lng = {44, 55, 66, 22};

        for (int i = 0; i < cars.length; i++) {
            if (cars[i] instanceof Car) {
                System.out.println("\nCar:");
                ((Car) cars[i]).addPassengers("Вася");
                cars[i].move(lat[i], lng[i]);
            } else if (cars[i] instanceof Bike) {
                System.out.println("\nBike:");
                ((Bike) cars[i]).crush();
                cars[i].move(lat[i], lng[i]);
            } else if (cars[i] instanceof Truck) {
                System.out.println("\nTruck:");
                ((Truck) cars[i]).loadTruck(1000);
                cars[i].move(lat[i], lng[i]);
            }
        }
    }
}
