package vehicles;

/**
 * Created by alekseykravchenko on 16.10.14.
 */
public abstract class Vehicle implements Moveable, Changable, Refuable {

    private int weight;
    private int height;
    private int width;
    private int wheelCount;
    private float speed;

    protected Vehicle() {
    }

    protected Vehicle(int weight, int height, int width, int wheelCount, float speed) {
        this.weight = weight;
        this.height = height;
        this.width = width;
        this.wheelCount = wheelCount;
        this.speed = speed;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getWheelCount() {
        return wheelCount;
    }

    public void setWheelCount(int wheelCount) {
        this.wheelCount = wheelCount;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public abstract void aging(int years);

    @Override
    public void changePart(int number) {
    }

    @Override
    public void move(int lat, int lng) {
    }

    @Override
    public void stop() {
    }

    @Override
    public void refuel(int liters) {
    }
}














