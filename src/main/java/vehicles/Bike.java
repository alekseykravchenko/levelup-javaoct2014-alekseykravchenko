package vehicles;

/**
 * Created by alekseykravchenko on 16.10.14.
 */
public class Bike extends Vehicle {

    public void crush() {
        System.out.println("Crashed!");
    }

    @Override
    public void aging(int years) {
        System.out.println("Aging to: " + years);
    }

    @Override
    public void changePart(int number) {
        System.out.println("Changing to: " + number);
    }

    @Override
    public void move(int lat, int lng) {
        System.out.println("Moving to: " + lat + ", " + lng);
    }

    @Override
    public void stop() {
        System.out.println("Stopping.");
    }

    @Override
    public void refuel(int liters) {
        System.out.println("Refuel: " + liters);
    }
}
