package jsp;

import jsp.dao.DatabaseUtilBean;
import jsp.dao.ModelResult;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alekseykravchenko on 20.01.15.
 */
public class RowInsertServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ModelResult result = (ModelResult) request.getSession().getAttribute("result");
        if (result!=null){
            String id = null;
            LinkedList<String> values = new LinkedList<String>();
            for (String columnName: result.getColumns()){
                String value = request.getParameter(columnName);
                if (!columnName.equalsIgnoreCase("id")){
                    values.add(value);
                } else {
                    id = value;
                }
            }
            List<String> columns = new LinkedList<String>();
            columns.addAll(result.getColumns());
            columns.remove("id");
            DatabaseUtilBean dbUtilBean = new DatabaseUtilBean();
            if (dbUtilBean.checkForRow(result.getTableName(), id)) {
                dbUtilBean.updateDataInTable(result.getTableName(), columns, values, id);
            } else {
                dbUtilBean.insertDataIntoTable(result.getTableName(), columns, values);
            }
            response.sendRedirect("show_table.jsp?table="+result.getTableName());
        }

    }
}