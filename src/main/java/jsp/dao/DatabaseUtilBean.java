package jsp.dao;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by alekseykravchenko on 20.01.15.
 */
public class DatabaseUtilBean {
    private Connection connection;

    public DatabaseUtilBean() {
        try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:/comp/env");
            DataSource ds = (DataSource) envContext.lookup("jdbc/TestDB");
            connection = ds.getConnection();
        } catch (NamingException ne){
            ne.printStackTrace();
        } catch (SQLException se){
            se.printStackTrace();
        }
    }

    public ModelResult selectFromTable(String tableName){
        ModelResult result = new ModelResult();
        try {
            String query = "SELECT * FROM " + tableName;
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            List<String> columns = new LinkedList<String>();
            ResultSetMetaData metaData = rs.getMetaData();
            for (int i = 1; i <= metaData.getColumnCount(); i++) {
                String columnName = metaData.getColumnName(i);
                columns.add(columnName);
            }
            List<List<String>> rowsList = new LinkedList<List<String>>();
            while (rs.next()){
                LinkedList<String> row = new LinkedList<String>();
                for (int i=1; i<=columns.size(); i++) {
                    String value = rs.getString(i);
                    row.add(value);
                }
                rowsList.add(row);
            }
            result.setTableName(tableName);
            result.setColumns(columns);
            result.setValues(rowsList);
            rs.close();
            stmt.close();
        } catch (SQLException e){
            e.printStackTrace();
        }
        return result;
    }

    public void insertDataIntoTable(String tableName, List<String> columns, List<String> values) {
        StringBuffer buffer  = new StringBuffer();
        buffer.append("INSERT INTO " + tableName +"(");
        boolean isFirst = true;
        for (String columnName: columns){
            if (isFirst){
                buffer.append(columnName);
                isFirst = false;
            } else {
                buffer.append(","+columnName);
            }
        }
        buffer.append(") VALUES(");
        isFirst = true;
        for (String value: values){
            if (isFirst){
                buffer.append("'"+value+"'");
                isFirst = false;
            } else {
                buffer.append(",'"+value+"'");
            }
        }
        buffer.append(");");
        String insertQuery = buffer.toString();
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(insertQuery);
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateDataInTable(String tableName, List<String> columns, List<String> values, String id) {
        StringBuffer buffer = new StringBuffer();
        buffer.append("UPDATE " + tableName + " SET ");
        boolean isFirst = true;
        for (String columnName : columns) {
            for (String value : values) {
                if (isFirst) {
                    buffer.append(columnName + "='" + value + "'");
                    isFirst = false;
                } else {
                    buffer.append(", " + columnName + "'" + value + "'");
                }
            }
        }
        buffer.append(" WHERE id=" + id);
        try {
            String updateQuery = buffer.toString();
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(updateQuery);
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean checkForRow(String tableName, String id) {
        String checkQuery = "SELECT * FROM " + tableName + " WHERE id=" + id;
        try {
            Statement stmt = connection.createStatement();
            ResultSet rs = stmt.executeQuery(checkQuery);
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}