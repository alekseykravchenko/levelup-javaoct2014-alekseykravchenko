package annotation;

/**
 * Класс Студент
 * Created by alekseykravchenko on 22.11.14.
 */
public class Reader {
    @Nullable(true)
    private String name;
    @Nullable(true)
    private Integer age;
    @Nullable(true)
    private Integer inn;
    @Nullable(false)
    private Boolean active;

    public Reader() {
    }

    public Reader(String name, Integer age, Integer inn, Boolean active) {
        this.name = name;
        this.age = age;
        this.inn = inn;
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getInn() {
        return inn;
    }

    public void setInn(Integer inn) {
        this.inn = inn;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }
}
