package annotation;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Write to Object using my annotation
 * Created by alekseykravchenko on 22.11.14.
 */
public class AnnotationAndHashMap {
    public static void main(String[] args) {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("name", "Aleksey");
        hashMap.put("age", 28);
        hashMap.put("inn", 2120406077);
        hashMap.put("active", true);

        try {
            Class aClass = Class.forName("annotation.Reader");
            Object reader = aClass.newInstance();
            Field[] fields = reader.getClass().getDeclaredFields();
            for (Field field : fields) {
                for (Map.Entry<String, Object> entry : hashMap.entrySet()) {
                    if (entry.getValue() != null && checkNullable(fields)) {
                        if (field.getName().equals(entry.getKey())) {
                            setValue(entry, field, aClass, reader);
                        }
                    }
                    if (entry.getValue() == null && checkNullable(fields)) {
                        System.out.println("Запрещенно писать в филду " + field.getName() + " null");
                        System.exit(1);
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private static void setValue(Map.Entry entry, Field field, Class aClass, Object reader) {
        String prefix = "set";
        String stringNameMethod = prefix + field.getName().replace(field.getName().charAt(0),
                field.getName().toUpperCase().charAt(0));
        try {
            Method methodName = aClass.getMethod(stringNameMethod, field.getType());
            methodName.invoke(reader, entry.getValue());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private static boolean checkNullable(Field[] fields) {
        for (Field field : fields) {
            if (field.getAnnotation(Nullable.class).value()) {
                return true;
            }
        }
        return false;
    }
}
