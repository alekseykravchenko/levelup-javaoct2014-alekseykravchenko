package autoCollection;

/**
 * Created by alekseykravchenko on 06.11.14.
 */
public class Auto {
    private String name;
    private String color;
    private String number;

    public Auto() {
    }

    public Auto(String name, String color, String number) {
        this.name = name;
        this.color = color;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String toString() {
        return "name=" + name + ", color=" + color + ", number=" + number;
    }
}
