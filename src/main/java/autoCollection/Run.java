package autoCollection;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * Тачки
 * Created by alekseykravchenko on 06.11.14.
 */
public class Run {
    public static void main(String[] args) {
        ArrayList<Auto> dasAutos = new ArrayList<Auto>();

        dasAutos.add(new Auto("BMW", "White", "AE2333AB"));
        dasAutos.add(new Auto("Mercedes", "Black", "AE1111AE"));
        dasAutos.add(new Auto("Toyota", "Green", "AE2222OB"));
        dasAutos.add(new Auto("Ford", "Blue", "AE2222OB"));
        dasAutos.add(new Auto("Fiat", "Blue", "AE2222OB"));
        dasAutos.add(new Auto("BMW", "Blue", "AE2222OB"));

        TreeSet<Auto> dasAutosColor = new TreeSet<Auto>(new ComparatorByColor());
        dasAutosColor.addAll(dasAutos);

        TreeSet<Auto> dasAutosNumber = new TreeSet<Auto>(new ComparatorByNumber());
        dasAutosNumber.addAll(dasAutosColor);

        TreeSet<Auto> dasAutosName = new TreeSet<Auto>(new ComparatorByName());
        dasAutosName.addAll(dasAutosNumber);

        for (Object e : dasAutosName) {
            System.out.println(e);
        }
    }

    private static class ComparatorByColor implements Comparator<Auto> {
        @Override
        public int compare(Auto o1, Auto o2) {
            return o1.getColor().compareTo(o2.getColor());
        }
    }

    private static class ComparatorByNumber implements Comparator<Auto> {
        @Override
        public int compare(Auto o1, Auto o2) {
            return o1.getNumber().compareTo(o2.getNumber());
        }
    }

    private static class ComparatorByName implements Comparator<Auto> {
        @Override
        public int compare(Auto o1, Auto o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

}
