package bricks;

/**
 * ДЗ 25.10.2014
 * Created by alekseykravchenko on 25.10.14.
 */

public class ThreeMethods {

    public static boolean makeBricks(int a, int b, int c) {
        if ((a * 1) + (b * 5) == c) {
            return true;
        }
        else {
            return false;
        }
    }

    public static int noTeenSum(int a, int b, int c) {
        return fixTeen(a) + fixTeen(b) + fixTeen(c);
    }

    public static int fixTeen(int a) {
        int[] stopList = {13, 14, 17, 18, 19};
        for (int i = 0; i < stopList.length; i++) {
            if (a == stopList[i]) {
                a = 0;
            }
        }
        return a;
    }

    public static int blackjack(int a, int b) {
        if (a == 0 || b == 0) return 0;
        if (a > b && a > 21) return b;
        if (a > b && a <= 21) return a;
        if (b > a && b > 21) return a;
        if (b > a && b <= 21) return b;
        if (a == b && a > 21) return 0;
        else return a;
    }

    public static void main(String[] args) {
        System.out.println("makeBricks(3, 1, 8): " + makeBricks(3, 1, 8));
        System.out.println("noTeenSum(11, 2, 19): " + noTeenSum(11, 2, 19));
        System.out.println("blackjack(19, 21): " + blackjack(19, 21));
    }
}
