package map;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by alekseykravchenko on 01.11.14.
 */
public class HashMapExample {

    public static void main(String[] args) {

        Map map = new HashMap();

        for (int i = 0; i <=1000; i++) {
            map.put(i + i*2 - 1, i);
        }
        System.out.println(map.containsKey("1"));
        System.out.println(map.containsValue("5"));

        for (Object key : map.keySet()) {
            System.out.println(map.keySet());
        }
    }
}
