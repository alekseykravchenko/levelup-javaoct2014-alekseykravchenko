package walkingTreeFiles;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.Stack;

/**
 * Написать аналог программы ls в unix.
 * Created by alekseykravchenko on 19.11.14.
 */
public class InputDirectory {
    public static void main(String[] args) throws IOException {
        run();
    }

    public static void run() throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите путь: ");
        String path = scanner.nextLine();
        isSourceExists(path);
    }

    public static void isSourceExists(String path) throws IOException {
        File sourceDirectory = new File(path);
        if (sourceDirectory.exists()) {
            findFilesAndDirectories(sourceDirectory);
        } else {
            System.out.println("Каталог не сущеттвует.");
        }
    }

    private static void findFilesAndDirectories(File sourceDirectory) throws IOException {
        Stack<File> stack = new Stack<File>();
        ArrayList<String> arrayList = new ArrayList<>();
        stack.push(sourceDirectory);
        while (!stack.isEmpty()) {
            File child = stack.pop();
            if (child.isDirectory()) {
                arrayList.add("Каталог: " + child.getName());
                for (File f : child.listFiles()) {
                    stack.push(f);
                }
            } else if (child.isFile()) {
                arrayList.add("Файл: " + child.getName());
            }
        }
        showFilesAndDirectories(arrayList);
    }

    public static void showFilesAndDirectories(ArrayList<String> arrayList) {
        Collections.sort(arrayList);
        for (String e : arrayList) {
            System.out.println(e);
        }
    }
}
