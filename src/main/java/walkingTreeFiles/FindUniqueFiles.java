package walkingTreeFiles;

import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.TreeSet;

/**
 * Записать в TreeSet уникальные имена файлов в каталоге и подкаталогах.
 * Created by alekseykravchenko on 18.11.14.
 */
public class FindUniqueFiles {
    public static void main(String[] args) throws IOException {
        String source = "/Users/alekseykravchenko/Desktop";
        File file = new File(source);
        findUniqueFiles(file);
    }

    public static void findUniqueFiles(File source) throws IOException {
        TreeSet<File> treeSet = new TreeSet<>(new FileNameComporator());
        if (source.isDirectory()) {
            File[] files = source.listFiles();
            for (File file : files) {
                if (file.isDirectory())
                    findUniqueFiles(file);
            }
            for (File file : files) {
                if (file.isFile()) {
                    treeSet.add(file.getCanonicalFile());
                }
            }
        }
        showUniqueFiles(treeSet);
    }

    public static void showUniqueFiles(TreeSet<File> treeSet) {
        for (File e : treeSet) {
            System.out.println(e.getName());
        }
    }

    private static class FileNameComporator implements Comparator<File> {
        @Override
        public int compare(File o1, File o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

}
