package dao;

/**
 * Category
 * Created by alekseykravchenko on 06.12.14.
 */
public class Category {

    public static final String INSERT_CATEGORY = "INSERT INTO `levelup`.`Category` (`categoryName`) VALUES (?);";
    private Long id;
    private String categoryName;

    public Category() {
    }

    public Category(Long id, String categoryName) {
        this.id = id;
        this.categoryName = categoryName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
