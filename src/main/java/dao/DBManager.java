package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Connection to MySQL
 * Created by alekseykravchenko on 06.12.14.
 */
public class DBManager {

    private static final String DB_URL = "jdbc:mysql://localhost:3306/levelup?useUnicode=true&characterEncoding=utf8";
    private static final String DB_USERNAME = "root";
    private static final String DB_PASSWORD = "qwerty123";
    private static final String DB_DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static DBManager instance;

    private DBManager() {
        try {
            Class.forName(DB_DRIVER_NAME);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    public Connection getConnection() throws SQLException {
        Connection connection = DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        return connection;
    }
}
