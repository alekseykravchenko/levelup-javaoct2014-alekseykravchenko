package dao;

/**
 * Product
 * Created by alekseykravchenko on 06.12.14.
 */
public class Product {

    public static final String INSERT_PRODUCT = "INSERT INTO `levelup`.`Product` " +
            "(`productName`, `productPrice`, `categoryId`) VALUES (?, ?, ?);";
    private Long id;
    private String productName;
    private Double productPrice;
    private Long categoryId;
    private Category category;

    public Product() {
    }

    public Product(Long id, String productName, Double productPrice, Long categoryId) {
        this.id = id;
        this.productName = productName;
        this.productPrice = productPrice;
        this.categoryId = categoryId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(Double productPrice) {
        this.productPrice = productPrice;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
