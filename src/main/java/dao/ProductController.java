package dao;

import java.sql.*;

/**
 * ProductController
 * Created by alekseykravchenko on 06.12.14.
 */
public class ProductController {

    private final static String SELECT_PRODUCT_QUERY = "SELECT Product.id AS product_Id, Product.productName, " +
            "Product.productPrice, \n" +
            "Product.categoryId AS category_Id, Category.categoryName\n" +
            "FROM Product JOIN Category ON (Category.id = Product.categoryId) \n" +
            "WHERE Category.categoryName = ?;";

    private DBManager dbManager = DBManager.getInstance();
    private Connection connection;
    private PreparedStatement categoryInsertStatement;
    private PreparedStatement productInsertStatement;
    private PreparedStatement productSelectStatement;

    public ProductController() {
        try {
            connection = dbManager.getConnection();
            categoryInsertStatement = connection.prepareStatement(Category.INSERT_CATEGORY, Statement.RETURN_GENERATED_KEYS);
            productInsertStatement = connection.prepareStatement(Product.INSERT_PRODUCT, Statement.RETURN_GENERATED_KEYS);
            productSelectStatement = connection.prepareStatement(SELECT_PRODUCT_QUERY);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Product addProduct(String productName, Double productPrice, String categoryName) {
        Product product = new Product();
        product.setProductName(productName);
        product.setProductPrice(productPrice);

        Category category = new Category();
        category.setCategoryName(categoryName);

        try {
            Long categoryId = saveCategory(category);
            product.setCategoryId(categoryId);
            Long productId = saveProduct(product);
            product.setId(productId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    private Long saveCategory(Category category) throws SQLException {
        Long id = null;
        categoryInsertStatement.setString(1, category.getCategoryName());
        categoryInsertStatement.executeUpdate();
        ResultSet resultSet = categoryInsertStatement.getGeneratedKeys();
        if (resultSet.next()) {
            id = resultSet.getLong(1);
        }
        return id;
    }

    private Long saveProduct(Product product) throws SQLException {
        Long id = null;
        productInsertStatement.setString(1, product.getProductName());
        productInsertStatement.setDouble(2, product.getProductPrice());
        productInsertStatement.setLong(3, product.getCategoryId());
        productInsertStatement.executeUpdate();

        ResultSet resultSet = productInsertStatement.getGeneratedKeys();
        if (resultSet.next()) {
            id = resultSet.getLong(1);
        }
        return id;
    }

    public Product getProductByCategory(String categoryName) {
        Category category;
        Product product = null;
        try {
            productSelectStatement.setString(1, categoryName);
            ResultSet resultSet = productSelectStatement.executeQuery();
            if (resultSet.next()) {
                category = new Category(resultSet.getLong("category_Id"), resultSet.getString("categoryName"));
                product = new Product(resultSet.getLong("product_Id"), resultSet.getString("productName"),
                        resultSet.getDouble("productPrice"), resultSet.getLong("category_Id"));
                product.setCategory(category);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    public void closeConnection() {
        try {
            categoryInsertStatement.close();
            productInsertStatement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
