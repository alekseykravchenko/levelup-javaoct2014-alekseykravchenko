package arrays;

/**
 * Работа с массивами
 * Created by alekseykravchenko on 12.10.14.
 */
public class RunArrayMethods extends ArrayMethods {

    public static void main(String[] args) {

        System.out.println("Задача 1. Создать массив заданной длины и заполнить его числами от 0:");
        createArray(4);

        System.out.println("\nЗадача 2. Дан массив int[]. Вернуть true, если есть 2 идущие одна за" +
                "другой цифры 2 или 4. Но не оба варианта:");
        System.out.println(either24(new int[]{1, 2, 2, 1, 4, 4}));

        System.out.println("\nЗадача 3. Дан массив int[]. Вернуть true, если в нем есть 1 и 2 после 1:");
        System.out.println(has12(new int[]{1, 4, 3, 1, 5, 2}));

        System.out.println("\nЗадача 4. Дан массив int[]. Вернуть true, если в нем есть две 7 подряд");
        System.out.println(twoTwo(new int[]{0, 7, 7, 8, 4, 7}));

        System.out.println("\nЗадача 5. Даны два числа start и end. Вернуть массив от start до end " +
                "(не включая):");
        createArray2(5, 10);

        System.out.println("\nЗадача 6. Дан непустой массив. Вернуть новый массив, который содержит" +
                "все элементы предыдущего до чила 4:");
        pre4(new int[]{1, 2, 3, 5, 4, 1});

        System.out.println("\nЗадача 7. Переставить в массиве все 0 вперед:");
        zeroFront(new int[]{1, 3, 8, 0, 5, 0, 0, 7});

        System.out.println("\nЗадача 8. Отсортировать в массиве четные и нечетные числа:");
        evenOdd(new int[]{1, 4, 6, 3, 6, 9, 5});

        System.out.println("\nЗадача 9. Вернуть сумму чисел массива, за исключением диапазона от 6 до 7:");
        System.out.println("Сумма: " + sum67(new int[]{1, 3, 6, 5, 5, 7, 1, 4}));

        System.out.println("\nЗадача 10. Вернуть true, если в массиве есть 3 подряд элементов, с увелечением " +
                "на 1:");
        System.out.println(tripleUp(new int[]{1, 6, 7, 8, 1, 3}));

    }
}
