package arrays;

/**
 * Класс методов по работе с массивами
 * Created by alekseykravchenko on 12.10.14.
 */
public class ArrayMethods {

    public static void createArray(int size) {
        String[] arr = new String[size];
        for (int i = 0; i < size; i++) {
            arr[i] = "" + i;
        }
        for (int i = 0; i < size; i++) {
            System.out.println(arr[i]);
        }
    }

    public static boolean either24(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == 2 && arr[i + 1] == 2) {
                count++;
            } else if (arr[i] == 4 && arr[i + 1] == 4) {
                count++;
            }
        }
        if (count == 1) return true;
        else return false;
    }

    public static boolean has12(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length - 2; i++) {
            if (arr[i] == 1 && arr[i + 2] == 2) {
                count++;
            }
        }
        if (count == 1) return true;
        else return false;
    }

    public static boolean twoTwo(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] == 7 && arr[i + 1] == 7) {
                count++;
            }
            if (arr[i] == 7 && arr[i + 1] != 7) {
                count--;
            }
        }
        if (count > 0) return true;
        else return false;
    }

    public static void createArray2(int start, int end) {
        int[] arr = new int[end];
        for (int i = 0; i < end; i++) {
            arr[i] = i;
        }
        for (int i = start; i < end; i++) {
            System.out.println(arr[i]);
        }
    }

    public static void pre4(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 4) break;
            count++;
        }
        int[] arr2 = new int[count];
        for (int i = 0; i < arr2.length; i++) {
            arr2[i] = arr[i];
        }
        for (int i = 0; i < arr2.length; i++) {
            System.out.println(arr2[i]);
        }
    }

    public static void zeroFront(int[] arr) {
        int tmp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] != 0) {
                    tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static void evenOdd(int[] arr) {
        int tmp;
        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] % 2 == 0) {
                    tmp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = tmp;
                }
            }
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static int sum67(int[] arr) {
        int find6 = 0;
        int find7 = 0;
        int sum67 = 0;
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 6) {
                find6 = i;
            }
            if (arr[i] == 7) {
                find7 = i;
            }
            sum += arr[i];
        }
        for (int i = find6; i <= find7; i++) {
            sum67 += arr[i];
        }
        return sum - sum67;
    }

    public static boolean tripleUp(int[] arr) {
        int count = 0;
        for (int i = 0; i < arr.length - 2; i++) {
            if (arr[i + 1] == arr[i] + 1 && arr[i + 2] == arr[i + 1] + 1) {
                count++;
            }
        }
        if (count == 1) return true;
        else return false;
    }

}
