package strings;

/**
 * Класс методов
 * Created by alekseykravchenko on 10.10.14.
 */
public class StringMethods {

    private String str1;
    private String str2;

    public StringMethods(String str1) {
        this.str1 = str1;
    }

    public StringMethods(String str1, String str2) {
        this.str1 = str1;
        this.str2 = str2;
    }

    public String wrapWord() {
        return str1.substring(0, 2) + str2 + str1.substring(2, 4);
    }

    public String firstHalf() {
        return str1.substring(0, str1.length() / 2);
    }

    public String nonStart() {
        return str1.substring(1) + str2.substring(1);
    }

    public boolean hasBad() {
        if (str1.substring(0, 3).contentEquals("bad")) return true;
        else return false;
    }

    public void withoutX() {
        if (str1.startsWith("x") && str1.endsWith("x"))
            System.out.println(str1.substring(1, str1.length() - 1));
        else if (str1.startsWith("x"))
            System.out.println(str1.substring(1));
        else if (str1.endsWith("x"))
            System.out.println(str1.substring(str1.length() - 1));
        else
            System.out.println(str1);
    }

    public void makeTags() {
        String openTag = "<".concat(str1).concat(">");
        String closeTag = "</".concat(str1).concat(">");
        System.out.println(openTag + str2 + closeTag);
    }

    public void atFirst() {
        if (str1.length() > 2)
            System.out.println(str1.substring(0, 2));
        else if (str1.length() < 2)
            System.out.println(str1 + "@");
    }

    public void comboString() {
        if (str1.length() < str2.length())
            System.out.println(str1 + str2 + str1);
        else if (str1.length() > str2.length())
            System.out.println(str2 + str1 + str2);
    }

    public void right2() {
        if (str1.length() > 2)
            System.out.println(str1.substring(str1.length() - 2)
                    + str1.substring(0, str1.length() - 2));
        else
            System.out.println("Символов меньше, чем 2!");
    }

    public void minCat() {
        if (str1.length() == str2.length())
            System.out.println(str1 + str2);
        else if (str1.length() > str2.length()) {
            int cut = str1.length() - str2.length();
            System.out.println(str1.substring(cut) + str2);
        } else if (str2.length() > str1.length()) {
            int cut = str2.length() - str1.length();
            System.out.println(str1 + str2.substring(cut));
        }
    }
}
