package strings;

/**
 * Точка входа в программу
 * Created by alekseykravchenko on 10.10.14.
 */
public class RunStringMethods {
    public static void main(String[] args) {

        StringMethods lab1 = new StringMethods("(())", "Hello");
        System.out.println("\nЗадача 1. Сделать обертку из первой строки:");
        System.out.println(lab1.wrapWord());

        StringMethods lab2 = new StringMethods("HelloWorld");
        System.out.println("\nЗадача 2. Вывести первую половину строки");
        System.out.println(lab2.firstHalf());

        StringMethods lab3 = new StringMethods("Hello", "World");
        System.out.println("\nЗадача 3. Вернуть две склеенные строки без бервых символов:");
        System.out.println(lab3.nonStart());

        StringMethods lab4 = new StringMethods("obaddy");
        System.out.println("\nЗадача 4. Вернуть true, если строка bad встречается в начале:");
        System.out.println(lab4.hasBad());

        StringMethods lab5 = new StringMethods("xTTTx");
        System.out.println("\nЗадача 5. Убрать символ x из начала и конца строки, если он есть:");
        lab5.withoutX();

        StringMethods lab6 = new StringMethods("html", "Hello");
        System.out.println("\nЗадача 6. Сделать из первой строки открывающий и закрывающий тег:");
        lab6.makeTags();

        StringMethods lab7 = new StringMethods("H");
        System.out.println("\nЗадача 7. Выводить первые 2 символа строки. Если символов меньше" +
                ", то заменить недостающие символы символом @:");
        lab7.atFirst();

        StringMethods lab8 = new StringMethods("Hi", "World");
        System.out.println("\nЗадача 8. Меньшая строка - обертка:");
        lab8.comboString();

        StringMethods lab9 = new StringMethods("java");
        System.out.println("\nЗадача 9. Последние 2 символа приклеить впереди и в зеркальном порядке:");
        lab9.right2();

        StringMethods lab10 = new StringMethods("HHHello", "java");
        System.out.println("\nЗадача 10. Обрезать одну из строк так, чтобы они были равны " +
                "(причем обрезать первые символы):");
        lab10.minCat();

    }
}
