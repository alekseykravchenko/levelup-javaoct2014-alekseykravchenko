package db;

import java.sql.*;

/**
 * Mysql Test read, write.
 * Created by alekseykravchenko on 29.11.14.
 */
public class DataBaseMysqlTest {

    public static void main(String[] args) {
        DataBaseMysqlTest test = new DataBaseMysqlTest();
        test.insertToTable();
        test.selectFromTable();
    }

    public Connection createConnection() throws SQLException {
        final String driverPath = "com.mysql.jdbc.Driver";
        final String url = "jdbc:mysql://localhost:3306/levelup?useUnicode=true&characterEncoding=utf8";
        final String username = "root";
        final String password = "qwerty123";
        try {
            Class.forName(driverPath);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(url, username, password);
    }

    public void selectFromTable() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = createConnection();
            statement = connection.createStatement();
            statement.execute("SELECT id, FirstName, LastName, Birthday, Phone, Active FROM levelup.Student;");
            resultSet = statement.getResultSet();
            while (resultSet.next()) {
                Integer id = resultSet.getInt(1);
                String firstName = resultSet.getString(2);
                String lastName = resultSet.getString(3);
                Date date = resultSet.getDate(4);
                String phone = resultSet.getString(5);
                Byte active = resultSet.getByte(6);
                System.out.println(id + "," + firstName + "," + lastName + "," + date + "," + phone + "," + active);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertToTable() {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = createConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        String updateString = "INSERT INTO levelup.Student (FirstName, LastName, " +
                "Birthday, Phone, Active) VALUES (?, ?, ?, ?, ?);";
        try {
            preparedStatement = connection.prepareStatement(updateString);
            preparedStatement.setString(1, "Алексей");
            preparedStatement.setString(2, "Кравченко");
            preparedStatement.setDate(3, new Date(new java.util.Date().getTime()));
            preparedStatement.setString(4, "+380931495629");
            preparedStatement.setByte(5, (byte) 1);
            preparedStatement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
